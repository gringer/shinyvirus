#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Shiny ViRus"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      radioButtons("transition1", label="Transition From:", 
                   choices = c("Susceptible"="S", "Infective / carrier"="I",
                               "Recovered / immune"="R", "Dead / away"="D"), 
                   selected = "S"),
      radioButtons("transition2", label="Transition To:", 
                   choices = c("Susceptible"="S", "Infective / carrier"="I",
                               "Recovered / immune"="R", "Dead / away"="D"), 
                   selected = "I"),
       sliderInput("transProb",
                   "Transition Probability:",
                   min = 0, max = 1, value = 0.1),
      tags$hr(style="size:4px"),
      sliderInput("popSize",
                  "Population Size (log scale):",
                  min = 0, max = 9, value = 4, pre="10^"),
      sliderInput("initI",
                  "Initial Infective Count:",
                  min = 0, max = 100, value = 1),
      sliderInput("modelDays",
                  "Days To Model:",
                  min = 0, max = 300, value = 120),
      actionButton("bReset", "Reset")
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
       plotOutput("distPlot"),
       plotOutput("nodePlot")
    )
  )
))
