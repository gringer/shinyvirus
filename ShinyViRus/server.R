#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(tidyverse)
library(ggplot2)
library(purrr)
library(igraph)

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
  
  values <- reactiveValues();
  
  resetProbs <- function(){
    values$transMat <- t(matrix(c(c(0.80,0.20,   0,   0), # S
                                  c(   0,0.88,0.10,0.02), # I
                                  c(   0,   0,   1,   0), # R
                                  c(   0,   0,   0,   1)),# D
                                nrow=4, ncol=4,
                                dimnames = list(c("S","I","R","D"),
                                                c("S","I","R","D"))));
  }

  output$distPlot <- renderPlot({
    ## Note: assumes constant population size
    if(is.null(values$transMat)){
      resetProbs();
    }
    numDays = input$modelDays;
    popSize = 10^input$popSize;
    currentVals <-
    t(c(S = popSize - input$initI,
      I = input$initI,
      R = 0,
      D = 0));
    currentVals -> res.mat;
    for(day in seq_len(numDays)){
      ## incorporate infective proportion into transitions from
      ## susceptivle to infective
      currentVals[,"I"] / popSize -> iProp;
      tTransMat <- values$transMat;
      tTransMat["S","I"] <- tTransMat["S","I"] * iProp;
      ## normalise to total population size
      tTransMat <- tTransMat / rowSums(tTransMat);
      currentVals %*% tTransMat -> currentVals;
      if(day < 3){
        print(day);
        print(iProp);
        print(tTransMat);
        print(currentVals);
      }
      rbind(res.mat, currentVals) -> res.mat;
    }
    as.tbl(as.data.frame(res.mat)) %>%
      mutate(day=(0:numDays), IRD=I + R + D) -> res.tbl;

    # draw the histogram with the specified number of bins
    res.tbl %>%
      pivot_longer(cols=-day, names_to="type", values_to="count") %>%
      ggplot() +
      aes(x=day, y=count, col=type) +
      geom_line(size=2) -> plt
    # spit out plotly output
    plt
  })
  
  output$nodePlot <- renderPlot({
    tm <- values$transMat;
    rownames(tm) <- colnames(tm) <- c("S"="Susceptible",
                                      "I"="Infective",
                                      "R"="Recovered",
                                      "D"="Dead")[colnames(tm)];
    ig <- graph_from_adjacency_matrix(tm, weighted=TRUE);
    plot(ig, layout=layout_in_circle, curved=TRUE,
         edge.label = edge_attr(ig, "weight"), edge.label.cex=0.71,
         vertex.size=60, vertex.size2=15, 
         vertex.shape="crectangle");
  });
  
  observeEvent(paste0(input$transition1, input$transition2), {
    t1 <- isolate(input$transition1);
    t2 <- isolate(input$transition2);
    updateSliderInput(session, "transProb", value=isolate(values$transMat[t1,t2]));
  });
  
  observeEvent(input$transProb, {
    ## TODO: update self->self transition according to other transitions
    ## [i.e. it gets what's left behind]
    t1 <- isolate(input$transition1);
    t2 <- isolate(input$transition2);
    values$transMat[t1,t2] <- input$transProb;
  });
  
  ## Observer functions
  observeEvent(input$bReset, {
    resetProbs();
    updateSliderInput(session, "initI", value = 1);
  })
  
})
